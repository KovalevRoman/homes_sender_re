import psycopg2
from settings import settings
import random
import csv
import codecs

def make():
    dblist = []
    with open('parsed_links.csv', "r", encoding="utf-8") as csvfile:
        x = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in x:
            a=row[0]
            linelist = []
            link = a.split(';')[ 0 ]
            cat = a.split(';')[1]
            linelist = [link, cat]
            dblist.append(linelist)
    return dblist

def sendtodb(pg_conn, list):
    pg_cursor = pg_conn.cursor()
    for i in list:
        i[1] = i[1].replace('-', ' ')
        # print(i[0],'-------', i[1])
        pg_cursor.execute(
            "INSERT INTO homes.homestable (profile_link, category, status) VALUES ('{}','{}','{}')".format(i[0], i[1], "Null"))
        print("Inserted")
    pg_conn.commit()

    return True



def main():
    # pass
    dblist = make()

    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print ("I am unable to connect to the database")

    sendtodb(pg_conn, dblist)

    pg_conn.close()



if __name__ == '__main__':
    main()
