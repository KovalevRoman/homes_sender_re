import re
import os
from time import sleep
import threading
from random import randint
import psycopg2
from settings import settings
import json
import requests
from requests.utils import requote_uri
import urllib.parse


def put_data(link, status):
    lock = threading.Lock()
    lock.acquire()
    try:
        try:
            pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                       "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
        except:
            print("Unable to connect to the database")

        pg_cursor = pg_conn.cursor()
        pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE profile_link = '{}'".format(status, link))
        pg_conn.commit()
        pg_conn.close()
    except:
        print("Unsuccesfull. Check database connection.")
    finally:
        lock.release()
    return link

def get_data():
    # sleep(randint(1, 3))
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    lock = threading.Lock()
    lock.acquire()
    try:
        pg_cursor.execute("BEGIN WORK")
        pg_cursor.execute("LOCK TABLE homes.homestable IN ACCESS EXCLUSIVE MODE")

        pg_cursor.execute(
            """SELECT profile_link FROM homes.homestable WHERE category='{}' AND status='Null' LIMIT 1""".format(settings.CATEGORY))


        link = pg_cursor.fetchone()[ 0 ]
        pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE profile_link = '{}'".format('Pending', link))
        pg_cursor.execute("COMMIT WORK")

    finally:
        lock.release()
    pg_conn.commit()
    pg_conn.close()
    sleep(1)
    return link

def remove_pendings():
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE status = '{}'".format('None', 'Pending'))
    pg_conn.commit()
    pg_conn.close()

def get_message_quantity():
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute("SELECT profile_link FROM homes.homestable WHERE category= '{}' AND status = '{}'".format(settings.CATEGORY, 'Null'))
    a = len(pg_cursor.fetchall())
    pg_conn.close()
    return a

def set_all_null():
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE status = '{}'".format(settings.CATEGORY, 'Sent'))
    pg_conn.commit()
    pg_conn.close()


def get_message():
    data = {}
    with open(os.path.dirname(__file__) + '/assets/message.txt') as data_file:
        # data = json.load(data_file)
        for i in data_file.readlines():
            line = i.split(":")
            if line[ 0 ] == '"name"':
                data[ 'name' ] = line[ 1 ][ 1:-3 ]
            if line[ 0 ] == '"email"':
                data[ 'email' ] = line[ 1 ][ 1:-3 ]
            if line[ 0 ] == '"phone"':
                data[ 'phone' ] = line[ 1 ][ 1:-3 ]
            if line[ 0 ] == '"time_to_call"':
                data[ 'time_to_call' ] = line[ 1 ][ 1:-3 ]
            if line[ 0 ] == '"message"':
                data['message'] = ''
                for i in range (1, len(line)):
                    data[ 'message' ] = data[ 'message' ] + ':' + line[i]
                if data[ 'message' ] [0] == ':':
                    data[ 'message' ] = data[ 'message' ][1:]
                data[ 'message' ] = data[ 'message' ][ 1:len(data[ 'message' ]) - 1 ]
                # print(data[ 'message' ])
    data_file.close()
    return data


def navigate(proxy, ua):
    message = get_message()
    profile = get_data()
    print("Link", profile, "is processing...")

    # only digits from link
    id = profile.split('/')[5].split('-')[1]
    name = urllib.parse.quote_plus(message['name'])
    email = urllib.parse.quote_plus(message['email'])
    phone = urllib.parse.quote_plus(message[ 'phone' ])
    mes = urllib.parse.quote_plus(message['message'])

    # print(mes)
    # print('--------------------------------')

    url = "http://www.homes.com/HomesCom/Include/Logic/ProviderProfile/providerLead.cfm?blocker1=&blocker2" \
               "=&fk_agentid={}&pma_id={}&name={}&email={}&phone={}&best_time=ASAP&comments={}"\
               "&instart_disable_injection=true".format(id, id, name, email, phone, mes)
    # print(url)
    proxies = {
        'http': 'http://'+proxy,
        'https': 'https://' + proxy
    }
    headers = {
        'User-Agent': ua,
    }
    try:
        pass
        r = requests.get(url, proxies=proxies, headers=headers)
        if re.search(r'"STATUSTEXT":"Success"', r.text):
            put_data(profile, "Sent")
            print("Message to", profile, "was sent!")
        else:
            print("Message to", profile, "was NOT sent!")
    except Exception as e:
        print("Exception: {} ".format(e))
        print("Error. Message to", profile, "was not send", )



def main():
    pass


if __name__ == '__main__':
    main()
