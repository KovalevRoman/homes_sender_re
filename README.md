# Скрипт для массовой рассылки личных сообщений на сайте недвижимости (США) #

### Используемые технологии ###
* Python 3
* [Python Requests library 2.18.4] (http://docs.python-requests.org/en/master/)
* База данных Postgres и адаптер [Psycopg2] (http://initd.org/psycopg/docs/)
* Python threading

### Features requests version ###
* Многопоточность и возможность выбора количества потоков
* Поддержка подмены user-agent.
* Работа с прокси-сервером или без него
* Вывод информации о выполнении программы в консоль

### Features Selenium version ###
* Работа с использованием Selenium.webdriver
* Поддержка Firefox, Chrome, PhantomJS браузеров (в том числи и в headless mode)
* Поддержка скриншотов, подтверждающих успешную отправку сообщения

Контакты: 

Email: rvk.sft[_at_]gmail.com