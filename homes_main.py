﻿import threading
import random

import homes
from settings import settings


class Bot():
    def __init__(self, proxy, ua):
        self.proxy = proxy
        self.ua = ua
        try:
            pass
            homes.navigate(self.proxy, self.ua)
        except Exception as e:
            print("Exception: {}".format(e))



class Thread_launcher (threading.Thread):
    def __init__(self, name, proxy, ua, thread_quantity, lock):
        threading.Thread.__init__(self)
        self.name = name
        self.proxy = proxy
        self.ua = ua
        self.threads = threads
        self.thread_quantity = thread_quantity
        self.lock = lock

    def run(self):
        global counter
        # print ("Starting thread " + self.name)

        a = Bot(self.proxy, self.ua)
        # print(self.proxy, self.ua)
        # print ("Exiting thread and deleting " + self.name)


        threads.append(self.name)
        self.lock.acquire()
        try:
            # Удаляем отработавший трид
            threads.pop(threads.index(self.name))
            # Счетчик запущенных тридов. Увеличивть при успешной отправке (то есть перенести в др место в коде(
            counter += 1
        finally:
            self.lock.release()  # освободить блокировку, что бы ни произошло
        if counter <= settings.MESSAGE_QUANTITY-settings.THREADS:
            if len(self.threads) <= self.thread_quantity: # Если тридов меньше, чем требуется в ТРЕАД
                tname = random.randint(100000, 999999) # Имя нового треда
                start_thread(proxylist, ualist, tname, lock, counter) # старт нового треда
        else:
            if counter == settings.MESSAGE_QUANTITY: # Заканчиваем работу, если послали все мессаги
                print("Messages were sent. Results were marked in databse \"Status\" column for each acc")
                print()
                print('==============================================')
                print('Press any key to exit console')
                print('==============================================')


            else: # Последние 5 мессат (не обязательо
                # print("Process is endings...")
                pass


    def decrease_counter(cnt):
        global counter
        lock.acquire()
        try:
            counter-=1
        finally:
            lock.release()


def init_thread_namelist(threadList):
    for i in range(settings.THREADS):
        threadList.append(random.randint(100000, 999999))
    return (threadList)


def start_thread(proxylist, ualist, tName, lock, counter):
    proxy_server = random.choice(proxylist)
    ua = random.choice(ualist)
    thread = Thread_launcher(tName, proxy_server, ua, settings.THREADS, lock)
    thread.start()


def init_first_thread_pack(proxylist, ualist, threadList, lock):
    for tName in threadList:
        start_thread(proxylist, ualist, tName, lock, counter)


def get_proxy_list():
    f = open(settings.PROXY_LIST_FILE, 'r')
    proxylist = []
    for i in f:
        i = i.replace('\n', '')
        proxylist.append(i)
    return proxylist


def get_ua_list():
    f = open(settings.UA_LIST_FILE, 'r')
    ualist = [ ]
    for i in f:
        i = i.replace('\n', '')
        ualist.append(i)
    return ualist


def main():
    print("Homes Sender is running...")

    homes.remove_pendings()
    if settings.MESSAGE_QUANTITY == 0:
        settings.MESSAGE_QUANTITY = homes.get_message_quantity()
    if settings.RESET_PROFILE == 0:
        homes.set_all_null()
        print ("All statuses in", settings.CATEGORY, "set to Null")

    global threads, ualist, proxylist, lock, counter
    counter = 0
    threads = []
    threadList = []
    lock = threading.Lock()
    proxylist = get_proxy_list()
    ualist = get_ua_list()

    threadList = init_thread_namelist(threadList)

    init_first_thread_pack(proxylist, ualist, threadList, lock)

    input()


if __name__ == '__main__':
    main()