# -*- coding: utf-8 -*-
import os
from sys import platform

from builtins import exec
import sys

IS_PROXY_ACCESS = 1

if "C:\\Program Files\\Homes_Sender\\homes_sender\\" not in sys.path:
    sys.path.append("C:\\Program Files\\Homes_Sender\\homes_sender\\")

'''Path to browser engines'''
BASE_PATH = os.path.join(os.path.dirname(__file__), '..')
C_PATH_TO_WEB_DRIVER = os.path.join(BASE_PATH, 'assets',
                                    'chromedriver.exe' if platform in ['win32', 'win34'] else 'chromedriver')

F_PATH_TO_WEB_DRIVER = 'C:\\Program Files\\Nightly\\firefox.exe'

# File with user-agents. User-agent on each line. ONLY desktop UA!
UA_LIST_FILE =  os.path.dirname(__file__) + '/../assets/useragents.txt'

# Databse settings
PG_SETTINGS = {
    'user': 'postgres',
    'password': 'changemeX2',
    'database': 'homes',
    'host': '127.0.0.1',
    'port': '5432'
}


PROXY_LIST_FILE =  os.path.dirname(__file__) + '/../assets/proxy.txt'

BROWSER = 'FH'
exec(open(BASE_PATH[:len(BASE_PATH)-2]+'user_settings.py').read())

RESET_PROFILE = 1
