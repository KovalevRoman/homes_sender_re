﻿import threading
import os
import random
from settings import settings
import homes_br
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import signal
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium import webdriver
import sys
from time import sleep


class Bot():
    def __init__(self, proxy, ua):
        self.proxy = proxy
        self.ua = ua
        try:

            driver = self.init_driver(self.proxy, self.ua)
            cnt = homes_br.navigate(driver)
            # driver.service.process.send_signal(signal.SIGTERM)  # kill the specific phantomjs child proc
            driver.quit()
            if cnt == 'Error':
                Thread_launcher.decrease_counter(cnt)
        except Exception as e:
            print("Exception in Bot() (Homes_main_br.py): {}".format(e))


    def init_driver(self,proxy,ua):
        sleep(random.randint(1000,1600)/1000)
        # if settings.BROWSER == "C":
        #     options = webdriver.ChromeOptions()
        #     if settings.IS_PROXY_ACCESS:
        #         options.add_argument('--proxy-server=http://' + proxy)
        #     options.add_argument('--user-agent=' + self.ua)
        #     # options.add_argument('--headless') ##Chrome 59 linux only
        #     driver = webdriver.Chrome(executable_path=settings.C_PATH_TO_WEB_DRIVER, chrome_options=options)
        # if settings.BROWSER == "P":
        #     dcap = dict(DesiredCapabilities.PHANTOMJS)
        #     dcap[ "phantomjs.page.settings.userAgent" ] = ua
        #     # dcap["phantomjs.page.settings.javascriptEnabled"] =  True
        #     service_args = []
        #     if settings.IS_PROXY_ACCESS:
        #         service_args = [
        #             '--proxy='+proxy,
        #             '--proxy-type=http',
        #         ]
        #     driver = webdriver.PhantomJS(desired_capabilities=dcap, service_args=service_args)
        if settings.BROWSER == "FH":
            fp = webdriver.FirefoxProfile()
            if settings.IS_PROXY_ACCESS == True:
                # Direct = 0, Manual = 1, PAC = 2, AUTODETECT = 4, SYSTEM = 5
                PROXY_PORT = proxy.split(':')[1]
                PROXY_HOST = proxy.split(':')[0]
                fp.set_preference("network.proxy.type", 1)
                fp.set_preference("network.proxy.http", PROXY_HOST)
                fp.set_preference("network.proxy.http_port", int(PROXY_PORT))
                fp.set_preference('network.proxy.ssl_port', int(PROXY_PORT))
                fp.set_preference('network.proxy.ssl', PROXY_HOST)
            fp.set_preference("general.useragent.override", ua)
            fp.set_preference("http.response.timeout", 60)
            fp.set_preference("dom.max_script_run_time", 60)
            fp.update_preferences()
            binary = FirefoxBinary(settings.F_PATH_TO_WEB_DRIVER, log_file=sys.stdout)
            # binary.add_command_line_options('-headless')
            # os.environ[ "MOZ_HEADLESS" ] = "1"
            driver = webdriver.Firefox(firefox_profile=fp, firefox_binary=binary)

        return driver

class Thread_launcher (threading.Thread):
    def __init__(self, name, proxy, ua, thread_quantity, lock):
        threading.Thread.__init__(self)
        self.name = name
        self.proxy = proxy
        self.ua = ua
        self.threads = threads
        self.thread_quantity = thread_quantity
        self.lock = lock

    def run(self):
        global counter
        # print ("Starting thread " + self.name)
        threads.append(self.name)
        a = Bot(self.proxy, self.ua)
        # print(self.proxy, self.ua)
        # print ("Exiting thread and deleting " + self.name)
        self.lock.acquire()
        try:
            # Удаляем отработавший трид
            threads.pop(threads.index(self.name))
            # Счетчик запущенных тридов. Увеличивть при успешной отправке (то есть перенести в др место в коде(
            counter += 1
        finally:
            self.lock.release()  # освободить блокировку, что бы ни произошло
        if counter <= settings.MESSAGE_QUANTITY-settings.THREADS:
            if len(self.threads) <= self.thread_quantity: # Если тридов меньше, чем требуется в ТРЕАД
                tname = random.randint(100000, 999999) # Имя нового треда
                start_thread(proxylist, ualist, tname, lock, counter) # старт нового треда
        else:
            if counter == settings.MESSAGE_QUANTITY: # Заканчиваем работу, если послали все мессаги
                print("Messages were sent. Results were marked in databse \"Status\" column for each acc")
                print()
                print('==============================================')
                print('Press any key to exit console')
                print('==============================================')

            else: # Последние 5 мессат (не обязательо
                # print("Process is endings...")
                pass


    def decrease_counter(cnt):
        global counter
        lock.acquire()
        try:
            counter-=1
        finally:
            lock.release()


def init_thread_namelist(threadList):
    for i in range(settings.THREADS):
        threadList.append(random.randint(100000, 999999))
    return (threadList)


def start_thread(proxylist, ualist, tName, lock, counter):
    proxy_server = random.choice(proxylist)
    ua = random.choice(ualist)
    thread = Thread_launcher(tName, proxy_server, ua, settings.THREADS, lock)
    thread.start()


def init_first_thread_pack(proxylist, ualist, threadList, lock):
    for tName in threadList:
        start_thread(proxylist, ualist, tName, lock, counter)


def get_proxy_list():
    f = open(settings.PROXY_LIST_FILE, 'r')
    proxylist = []
    for i in f:
        i = i.replace('\n', '')
        proxylist.append(i)
    return proxylist


def get_ua_list():
    f = open(settings.UA_LIST_FILE, 'r')
    ualist = [ ]
    for i in f:
        i = i.replace('\n', '')
        ualist.append(i)
    return ualist


def main():
    homes_br.remove_pendings()
    if settings.MESSAGE_QUANTITY == 0:
        settings.MESSAGE_QUANTITY = homes_br.get_message_quantity()
    if settings.RESET_PROFILE == 0:
        homes_br.set_all_null()
        print ("All statuses in", settings.CATEGORY, "set to Null")

    global threads, ualist, proxylist, lock, counter
    counter = 0
    threads = []
    threadList = []
    lock = threading.Lock()
    proxylist = get_proxy_list()
    ualist = get_ua_list()

    threadList = init_thread_namelist(threadList)

    init_first_thread_pack(proxylist, ualist, threadList, lock)

    input()


if __name__ == '__main__':
    main()
