﻿from random import randint
from time import sleep
import threading
from random import randint
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import psycopg2
from settings import settings
import json
import os


def put_data(link, status):
    lock = threading.Lock()
    lock.acquire()
    try:
        try:
            pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                       "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
        except:
            print("Unable to connect to the database")

        pg_cursor = pg_conn.cursor()
        pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE profile_link = '{}'".format(status, link))
        pg_conn.commit()
        pg_conn.close()
    except:
        print("Unsuccesfull. Check database connection.")
    finally:
        lock.release()
    return link

def get_data():
    """
    1. Взять строку из db
    2. Отметить как Pending
    3. Отметить как еррор или как саксесс
    """
    
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute(
        """SELECT profile_link FROM homes.homestable WHERE category='{}' AND status='Null' LIMIT 1""".format(settings.CATEGORY))
    link = pg_cursor.fetchone()[ 0 ]
    pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE profile_link = '{}'".format('Pending', link))
    pg_conn.commit()
    pg_conn.close()
    sleep(1)

    return link

def remove_pendings():
    """
    1. Взять строку из db
    2. Отметить как Pending
    3. Отметить как еррор или как саксесс
    """

    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE status = '{}'".format('None', 'Pending'))
    pg_conn.commit()
    pg_conn.close()

def get_message_quantity():
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute("SELECT profile_link FROM homes.homestable WHERE category= '{}' AND status = '{}'".format(settings.CATEGORY, 'Null'))
    a = len(pg_cursor.fetchall())
    pg_conn.close()
    return a

def set_all_null():
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("Unable to connect to the database")
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute("UPDATE homes.homestable SET status = '{}' WHERE status = '{}'".format(settings.CATEGORY, 'Sent'))
    pg_conn.commit()
    pg_conn.close()

def get_message():
    try:
        with open(os.path.dirname(__file__) + '/assets/message.txt') as data_file:
            data = json.load(data_file)
        data_file.close()
        return data
    except Exception as e:
        print("Exception: {} Message".format(e))

def navigate(driver):
    wait = WebDriverWait(driver, 50)

    try:
        lock = threading.Lock()
        lock.acquire()
        try:
            link = get_data()
        finally:
            lock.release()
        message = get_message()
        driver.get(link)
        print('Processing link:', link)
        
        element = wait.until(EC.presence_of_element_located(
            (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/input[5]")))
        element.click()
        element.send_keys(message['name'])

        element = wait.until(EC.presence_of_element_located(
            (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/input[6]")))
        element.click()
        element.send_keys(message['email'])

        element = wait.until(EC.presence_of_element_located(
            (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/input[7]")))
        element.click()
        element.send_keys(message['phone'])

        element = wait.until(EC.presence_of_element_located(
            (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/textarea")))
        element.click()
        element.send_keys(message['message'])

        if message['time_to_call'] == 'ASAP':
            element = wait.until(EC.presence_of_element_located(
                (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/div[2]/label[1]/input")))
            element.click()

        if message[ 'time_to_call' ] == 'Morning':
            element = wait.until(EC.presence_of_element_located(
                (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/div[2]/label[2]/input")))
            element.click()

        if message[ 'time_to_call' ] == 'Afternoon':
            element = wait.until(EC.presence_of_element_located(
                (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/div[2]/label[3]/input")))
            element.click()

        if message[ 'time_to_call' ] == 'Evening':
            element = wait.until(EC.presence_of_element_located(
                (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/div[2]/label[4]/input")))
            element.click()

        element = wait.until(EC.presence_of_element_located(
            (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/div[3]/input")))
        element.click()

        element = wait.until(EC.presence_of_element_located(
            (By.XPATH, u"/html/body/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/form/p")))

        put_data(link,status="Sent")
        if settings.TAKE_SCREENSHOTS == 1:
            # print(os.path.dirname(__file__))
            driver.save_screenshot(os.path.dirname(__file__) + "/screenshots/"+ link.split('/')[5]+".png")
            print("Got screenshot:", link.split('/')[5])

        print("Message to", link, "was sent succesfull")


    except Exception as e:
        print("Exception: {} Message {}".format(e))
        print("Error. Message to", link, "was not send", )
        put_data(link, status="Null")
        return 'Error'



def main():
    pass
    get_message_quantity()

if __name__ == '__main__':
    main()
